#!/bin/bash

JAVA="java -Xmx32g"
#TEST_DIR=../testprograms/qualitas/
TEST_DIR=../testprograms/qualitas/

if [ ! -f "results.csv" ] ; then
	echo "Domain,Analysis,Internal,JavaFiles,Nodes,Edges,NodesAndEdges,SCCs,FullTime,ParseTime,GenerationTime,AnalysisTime,GenAnaTime,Run,Scenario,TimeTime,Exit" > "results.csv"
fi

for i in {0..100} ; do


	# Transform
	echo "Transform model with n=$i" ;
	for f in `ls $TEST_DIR` ; do
	    # if [[ $f == "netbeans" || $f == "eclipse_SDK" ]]; then
	    #     echo "skipping $f"
        #     continue
        # fi
	    echo "processing directory: " $f "..."
		echo package     internal $TEST_DIR$f
		/usr/bin/time -o "results.csv" -a -f "$i,$f,%e,%x" ../extendj/java8relast/build/install/java8relast/bin/java8relast package     internal $TEST_DIR$f >> results.csv
		echo package     external $TEST_DIR$f
		/usr/bin/time -o "results.csv" -a -f "$i,$f,%e,%x" ../extendj/java8relast/build/install/java8relast/bin/java8relast package     external $TEST_DIR$f >> results.csv
		echo type        internal $TEST_DIR$f
		/usr/bin/time -o "results.csv" -a -f "$i,$f,%e,%x" ../extendj/java8relast/build/install/java8relast/bin/java8relast type        internal $TEST_DIR$f >> results.csv
		echo type        external $TEST_DIR$f
		/usr/bin/time -o "results.csv" -a -f "$i,$f,%e,%x" ../extendj/java8relast/build/install/java8relast/bin/java8relast type        external $TEST_DIR$f >> results.csv

	done

done
