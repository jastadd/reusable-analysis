/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.tudresden.inf.st.reusablecfg;

import org.extendj.ExtendJVersion;
import org.extendj.ast.CompilationUnit;
import org.extendj.ast.ExtendJFinding;
import org.extendj.ast.Frontend;
import org.extendj.ast.Program;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Produces findings using analyzers implemented in the ExtendJ compiler.
 */
public class ExtendJAnalyzerFrontend extends Frontend {

  private final Collection<ExtendJFinding> findings = new ArrayList<>();

  public ExtendJAnalyzerFrontend() {
    super("Simple CFG Checker", ExtendJVersion.getVersion());
  }

  /**
   * Analyze a single file for findings and return the findings in a collection.
   */
  public static Collection<ExtendJFinding> analyzeFile(final String path) throws Error {
    ExtendJAnalyzerFrontend checker = new ExtendJAnalyzerFrontend();
    int result = checker.run(new String[]{path});
    if (result != EXIT_SUCCESS) {
      throw new Error("exit code: " + result);
    }
    return checker.findings;
  }

  /**
   * Returns the list of findings from the analyzed source files.
   *
   * <p>Used by ExtendJAnalyzerMain to print the generated findings on stdout.
   */
  Collection<ExtendJFinding> getFindings() {
    return findings;
  }

  /**
   * Run the Java checker.
   *
   * @param args command-line arguments
   * @return 0 on success, 1 on error, 2 on configuration error, 3 on system
   */
  public int run(String[] args) {
    return run(args, Program.defaultBytecodeReader(), Program.defaultJavaParser());
  }

  @Override
  protected int processCompilationUnit(CompilationUnit unit) {
    super.processCompilationUnit(unit);
    if (unit.fromSource()) {
      findings.addAll(unit.findings());
    }
    return EXIT_SUCCESS;
  }
}
