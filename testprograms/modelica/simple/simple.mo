within ModelicaCompliance.Scoping.InnerOuter;

model SimpleNameLookup
  extends Icons.TestCase;

  class A
    outer Integer T0;
  end A;

  class B
    inner Integer T0 = 10;
    A a1, a2; // B.T0, B.a1.T0 and B.a2.T0 is the same variable
  end B;

  B b;
equation
end SimpleNameLookup;
