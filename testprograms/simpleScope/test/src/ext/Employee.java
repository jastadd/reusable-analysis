package ext;

import core.Person;

public class Employee extends Person {
	private int id; //shadows Person::id(4)
	public Employee(int i,String n) {super(i,n);id=i;}
	public int getId() {return id+1000;}
	public String getName() {return "E "+name;}
	
	private class Memo {
		private Memo(int i,String n){id=i;name=n;}
		private final int id;//shadows id(6)
		private final String name;//shadows Person::name(5)
	}	
	public Memo save() {return new Memo(id,name);}
	public void restore(Memo m) {id=m.id; name=m.name;}
}
