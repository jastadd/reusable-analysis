package core;

public class Person {
	public final int id;
	protected String name;
	public Person(int i,String n) {name=n; id=i;}//shadows i(11)
	public int getId() {return id;}
	public String getName() {return name;}
	public void setName(String name) {this.name = name;} //shadows name(5)

	int i;
	public void work() {
		for (i = 0; id < 10; i++) {/* ... */}
		for (int i = 0; i < 10; i++) {/* ... */}//shadows i(14)
	}
}
