#!/usr/bin/env python3
import os
import subprocess


def main(start_dir):
    try:
        for directory, subdirectories, files in os.walk(start_dir):
            if 'disabled' in subdirectories:
                subdirectories.remove('disabled')
            for file in files:
                filename = directory + '/' + file
                # print(filename)
                subprocess.check_call(['./gradlew', '-q', 'run', '--args="-q" "' + filename + '"'])
                # 1/0
    except Exception:
        print('Gotcha')


if __name__ == '__main__':
    main('src/test/resources/modelica')
