package org.jmodelica;

import org.jmodelica.compiler.ModelicaCompiler;
import org.jmodelica.compiler.ParserHandler;
import org.jmodelica.compiler.SourceRoot;
import org.jmodelica.util.exceptions.CompilerException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Main class to start parsing a Modelica file.
 *
 * @author rschoene - Initial contribution
 */
public class SimpleMain {
  public static void main(String[] args) throws Exception {
    final String filename;
    final boolean verbose;
    switch (args.length) {
      case 1:
        // use first argument as filename
        filename = args[0];
        verbose = true;
        break;
      case 2:
        if (args[0].equals("-q")) {
          filename = args[1];
          verbose = false;
        } else if (args[1].equals("-q")) {
          filename = args[0];
          verbose = false;
        } else {
          System.err.println("Ignoring 2nd parameter '" + args[1] + "'");
          filename = args[0];
          verbose = true;
        }
        break;
      default:
        System.err.println("Missing filename to parse! Exiting.");
        System.exit(1);
        return;
    }
    if (verbose) System.out.println("You are here: '" + new File(".").getAbsolutePath() + "'.");
    SourceRoot sourceRoot = parseModel(filename);
    String pretty = sourceRoot.prettyPrint("");
    if (verbose) {
      System.out.println("Contents of '" + filename + "':");
      System.out.println(pretty);
    }
  }

  private static SourceRoot parseModel(String name)
      throws IOException, beaver.Parser.Exception, CompilerException {
    ModelicaCompiler mc = new ModelicaCompiler(ModelicaCompiler.createOptions());
    return mc.parseModel(name);
  }

}
