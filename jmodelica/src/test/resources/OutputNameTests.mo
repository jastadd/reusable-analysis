package NameTests
 model NameTest2
  class A
   class B
    Real x;

    class C
     Real y;
    equation
y = 2;
    end C;

    C c;
   end B;

   B b, b1;
   B.C c;
   Real y, x;
  equation
x = -1/(1)*1*1-(2+1)^(3)+x^(-3+2);
b.c.y+1 = b.c.y;
c.y = 0;
b.x = b.c.y;
  end A;

  A a;
 end NameTest2;

 model NameTest3_Err
  A a;
 end NameTest3_Err;

 model NameTest4_Err
  model M
   model A
    Real x = 3;
   end A;

   B a;
  end M;

  M m;
 end NameTest4_Err;

 model NameTest5_Err
  model A
   Real y = 4;
  end A;

  A a;
  Real y;
 equation
b.y = y+a.x;
 end NameTest5_Err;

 model NameTest55_Err
  model A
   Real y = 4;
  equation
y = x;
  end A;

  A a;
 end NameTest55_Err;

 model NameTest6_Err
  model A
   Real x = y;
  end A;

  A a;
 end NameTest6_Err;

 model NameTest7_Err
  model A
   B x;
  end A;

  A a1;
  A a2;
 end NameTest7_Err;

 model NameTest8_Err
  model C = D
  C c;
 end NameTest8_Err;

 model NameTest9_Err
  model A
   Real x = 4;
  end A;

  model B
   Real x = 6;
   Real y = 7;
  end B;

  model C
   replaceable B b constrainedby A;
  end C;

  C c(b(y = 3));
 end NameTest9_Err;

 model NameTest10_Err
  package P1
   model A
    Real x = 4;
   end A;
  end P1;

  package P2
   model A
    Real x = 4;
   end A;

   model B
    Real x = 6;
    Real y = 7;
   end B;
  end P2;

  replaceable package P = P2 constrainedby P1
  P.B b;
 end NameTest10_Err;

 model NameTest11_Err
  model A
   parameter Real p1 = 4;
  end A;

  parameter Real p = 5;
  A a(p1 = p1);
 end NameTest11_Err;

 model NameTest12_Err
  model M
   model A
    Real x = 4;
   end A;

   model B
    Real x = 4;
    Real y = 4;
   end B;

   replaceable A a;
  end M;

  M m(redeclare B a);
 end NameTest12_Err;

 model NameTest13_Err
  package P
   model A
    Real x = 1;
   end A;

   model B
    Real x = 2;
    Real y = 3;
   end B;

   model C
    Real x = 2;
    Real y = 3;
    Real z = 4;
   end C;

   replaceable model BB = B(z = 3)  end P;

  package PP = P(   redeclare model BB
        extends C(y = 4);
   end BB;
)
  PP.BB bb(y = 6);
 end NameTest13_Err;

 model NameTest14_Err
  package P
   model A
    Real x = 1;
   end A;

   model B
    Real x = 2;
    Real y = 3;
   end B;

   model C
    Real x = 2;
    Real y = 3;
    Real z = 4;
   end C;

   replaceable model BB
        extends B(z = pBB);
   end BB;
  end P;

  package PP = P(   redeclare replaceable model BB = P.B(z = p))
  PP.BB bb(z = pp);
 end NameTest14_Err;

 class NameTest15
 protected
  Real x = 1;
 end NameTest15;

 class NameTest16
  constant Real c = 1.0;
  parameter Real p = c;
 end NameTest16;

 model NameTest17
  Real x(fixed, start);
 equation
x = 2;
 end NameTest17;

 model NameTest18
  package A
   package C
    model M
     Real x;
    end M;
   end C;

   package D
        extends C;
   end D;
  end A;

  package B
   model M
    Real x;
   end M;
  end B;

  package E
   replaceable package F = B constrainedby A.D  end E;

  E.F.M y;
 end NameTest18;

 model NameTest19
  package A
   constant Integer b = 1;
  end A;

  model B
   Real x[1];
  end B;

  B y;
 equation
y.x[A.b] = 1;
 end NameTest19;

 model NameTest20
  package A
   function f
    output Real z;
   algorithm
    z:=1;
   end f;

   model B
    Real x;
   equation
x = f();
   end B;
  end A;

  model C
      extends A.B;
  end C;

  C y;
 end NameTest20;

 model NameTest21
  model A
   replaceable package B = C
   J c(   redeclare package I = B);
  end A;

  package C
   constant Real a = 1;
  end C;

  model D
   package E = F
   A b(   redeclare package B = E.G);
  end D;

  package F
   package G
        extends C(a = 2);
   end G;
  end F;

  model J
   replaceable package I = C
   Real x = I.a;
  end J;

  D d;
 end NameTest21;

 model NameTest22
  model C
   constant Real a = 1;
  end C;

  package F
   package G
        extends C(a = 2);
   end G;
  end F;

  model D
   package E = F
   package B = E.G(a = 3)
   package I = B
   Real x = I.a;
  end D;

  D d;
 end NameTest22;

 model NameTest23
  model A
   Real x;
  end A;

  model B
   parameter Integer n;
   A[n] a;
   Real[n] y = a.x;
  end B;

  model C
   parameter Integer m;
   B[m] b(n = 1:m);
  end C;

  C c(m = 4);
 end NameTest23;

 model NameTest24
  model A
   replaceable B b constrainedby B;
  end A;

  model B
   Real x;
  end B;

  model C
   Real x = 1;
   Real y = x;
  end C;

  A a(redeclare C b);
  Real z = a.b.y;
 end NameTest24;

 model NameTest25_Err
  model A
   replaceable B b constrainedby B;
  end A;

  model B
   Real x;
  end B;

  model C
   Real x = 1;
   Real y = x;
  end C;

  A a(redeclare replaceable C b);
  Real z = a.b.y;
 end NameTest25_Err;

 model NameTest26
  model A
   Real x = time;
  end A;

  model B = A(x(start = c))
  parameter Real c = 2;
  B b;
 end NameTest26;

 model NameTest27
  model A
   Real x = time;
  end A;

  model D
   model B = A(x(start = c))
   parameter Real c = 2;
   B b;
  end D;

  D d(c = 3);
 end NameTest27;

 model NameTest28
  model A
   Real x = time;
  end A;

  model D
   replaceable model B = A
   B b;
  end D;

  model E
   parameter Real c = 2;
   D d(   redeclare model B = A(x(start = c)));
  end E;

  E e(c = 3);
 end NameTest28;

 constant Real constant_1 = 1.0;

 class ConstantLookup1
  Real x = constant_1;
 end ConstantLookup1;

 class ConstantLookup2
  model Inner
   Real x = constant_1;
  end Inner;

  Inner i;
 end ConstantLookup2;

 class ConstantLookup3
  constant Real constant_1 = 2.0;

  model Inner
   Real x = constant_1;
  end Inner;

  Inner i;
 end ConstantLookup3;

 class ConstantLookup4
  parameter Real p = Modelica.Constants.pi;
 end ConstantLookup4;

 class ConstantLookup5
  import Modelica.Constants.*;
  parameter Real p = pi;
 end ConstantLookup5;

 class ConstantLookup6
  import C = Modelica.Constants;
  parameter Real p = C.pi;
 end ConstantLookup6;

 class ConstantLookup7
  import Modelica.*;
  parameter Real p = Constants.pi;
 end ConstantLookup7;

 class ConstantLookup8
  import pi2 = Modelica.Constants.pi;
  parameter Real p = pi2;
 end ConstantLookup8;

 class ConstantLookup9
  import Modelica.Constants.pi;
  parameter Real p = pi;
 end ConstantLookup9;

 model ConstantLookup10
  package P
   parameter Real x;
  end P;

  import NameTests.ConstantLookup10.P.x;
  parameter Real p = x;
 end ConstantLookup10;

 model ConstantLookup11
  package P
   parameter Real x;
  end P;

  import x2 = NameTests.ConstantLookup11.P.x;
  parameter Real p = x2;
 end ConstantLookup11;

 model ConstantLookup12
  package P
   parameter Real x;
  end P;

  import NameTests.ConstantLookup12.P.*;
  parameter Real p = x;
 end ConstantLookup12;

 class ConstantLookup13
  package P
  protected
   constant Real prot = 1.0;
  end P;

  import NameTests.ConstantLookup13.P.*;
  parameter Real p = prot;
 end ConstantLookup13;

 class ConstantLookup14
  package P
  protected
   constant Real prot = 1.0;
  end P;

  import NameTests.ConstantLookup14.P.prot;
  parameter Real p = prot;
 end ConstantLookup14;

 class ConstantLookup15
  package P
  protected
   constant Real prot = 1.0;
  end P;

  import prot2 = NameTests.ConstantLookup15.P.prot;
  parameter Real p = prot2;
 end ConstantLookup15;

 model ConstantLookup16
  constant Real a = b[c];
  constant Real[3] b = {1,2,3};
  constant Integer c = d;
 end ConstantLookup16;

 model ConstantLookup17
  package A
   parameter Integer n = 2;
  end A;

  model B
   constant Integer n = 3;
  end B;

  Real a = A.n;
  Real b = B.n;
 end ConstantLookup17;

 model ConstantLookup18
  model A
   parameter Integer n;
   Real x[n] = fill(1,n);
  end A;

  model B
      extends A(n = C.f.n);

   replaceable package C = D  end B;

  package D
      extends E;
  end D;

  package E
   constant F f = F(3);
  end E;

  record F
   Integer n;
  end F;

  B b;
 end ConstantLookup18;

 model ConstantLookup19
  package A
   constant Integer n;

   model AA
    Real x[n] = fill(1,n);
   end AA;
  end A;

  package B
      extends A(n = C.f.n);

   replaceable package C = D  end B;

  package D
      extends E;
  end D;

  package E
   constant F f;
  end E;

  record F
   Integer n;
  end F;

  package G
      extends B(   redeclare package C = H);
  end G;

  package H
      extends E(f(n = 3));
  end H;

  G.AA y;
 end ConstantLookup19;

 model ConstantLookup20
  package A
   constant B b = B(3);
  end A;

  record B
   Real x;
  end B;

  Real y = A.b.x;
 end ConstantLookup20;

 model ConstantLookup21
  model A
   replaceable package B = C constrainedby D
   Real x[B.n] = ones(B.n);
  end A;

  package C
   constant Integer n = 2;
  end C;

  package D
   constant Integer n;
  end D;

  A a;
 end ConstantLookup21;

 model ConstantLookup22
  partial package A
   partial replaceable model M = B  end A;

  partial model B
   replaceable package N = E
   input Real[N.b] a;
  end B;

  model C
      extends B(   redeclare replaceable package N = F constrainedby G);
  end C;

  package D
      extends F;
      extends A(   redeclare model M = C(    redeclare package N = F));
  end D;

  partial package E
   constant Integer b(min = 1);
  end E;

  package F
      extends G(   redeclare package O = H);
  end F;

  package G
      extends E(b = O.c.g);

   replaceable package O = J  end G;

  package H
      extends I(c(g = 2));
  end H;

  package I
      extends J;
  end I;

  package J
   constant K c;
  end J;

  record K
   parameter Integer g;
  end K;

  model L
   replaceable package P = D constrainedby M
   P.M d(a = e);
   Real[P.b] e = ones(P.b);
  end L;

  package M
      extends A;
      extends E;
  end M;

  L f;
 end ConstantLookup22;

 model ConstantLookup23
  package A
  end A;

  package B
   constant Real x = 1.0;
  end B;

  replaceable package C = B constrainedby A
  Real y = C.x;
 end ConstantLookup23;

 model ConstantLookup24
  package A
   constant Real d = 1.0;
  end A;

  package B
   constant Real d;
  end B;

  package C
      extends A;
      extends B;
  end C;

  Real x = C.d;
 end ConstantLookup24;

 model ConstantLookup25
  record A
   Real a;
  end A;

  constant A x[:] = {A(1),A(2)};
  constant Real y[2] = x.a;
  Real z[2] = y;
 end ConstantLookup25;

 package ExtraForConstantLookup26
  partial package A
      extends B(d = size(b,1), c = b[:].a);

   record C
    Real a;
   end C;

   constant C[:] b;
  end A;

  package B
   constant Integer d;
   constant Real[d] c;
  end B;

  package D
      extends A;
  end D;

  package E
   constant F.G[:] e = {F.f,F.g};
      extends D(b = e);
  end E;

  package F
   record G
    Real a;
   end G;

   constant G f(a = 1);
   constant G g(a = 2);
  end F;
 end ExtraForConstantLookup26;

 model ConstantLookup26
  constant Real x[2] = ExtraForConstantLookup26.E.c;
  Real y[2] = x;
 end ConstantLookup26;

 model ConstantLookup27
  record A
   Real a;
   Real b;
  end A;

  constant A c = A(1,2);

  package B
   constant A d = A(3,4);
  end B;

  package C
      extends B(d = c);
  end C;

  constant Real e[2] = {C.d.a,C.d.b};
  Real f[2] = e;
 end ConstantLookup27;

 model ConstantLookup28
  package A
   constant Real a = 2;
  end A;

  package B
      extends C;

   model D
    Real x = a;
   end D;
  end B;

  package C
      extends A;
  end C;

  B.D y;
 end ConstantLookup28;

 model ConstantLookup29
  package A
   constant Real c1 = 1;
  end A;

  model B
   import NameTests.ConstantLookup29.A.*;
   Real x(start = c1) = 2;
  end B;

  model C
      extends B;
  end C;

  C a;
 end ConstantLookup29;

 model ConstantLookup30
  package Constants
   constant Real c = 3.1;
  end Constants;

  model M1
   import NameTests.ConstantLookup30.Constants.*;
   import SI = Modelica.SIunits;
   parameter Real p = c;
   SI.Force f = 1;
  end M1;

    extends M1;
 end ConstantLookup30;

 package ConstantLookup31
  constant Integer c = 1;

  package NameTests
   package ConstantLookup31
    constant Integer c = 2;

    model ConstantLookup31_m
     constant Integer a = .NameTests.ConstantLookup31.c;
     constant Integer b = NameTests.ConstantLookup31.c;
    end ConstantLookup31_m;
   end ConstantLookup31;
  end NameTests;
 end ConstantLookup31;

 model ConstantLookup32
  record A
   Real b;
  end A;

  package C
   constant A[2] d = {A(3),A(4)};
  end C;

  function f
   input Integer i;
   output Real x;
  algorithm
   x:=C.d[i].b;
  end f;

  parameter Integer j = 1;
  Real y = f(j);
 end ConstantLookup32;

 model ConstantLookup33
  package D
   record A
    Real b;
   end A;
  end D;

  package F
   constant D.A[2] d;
  end F;

  package C
   package E = D
   constant E.A g = E.A(3);
   constant E.A h = E.A(4);
      extends F(d = {g,h});
  end C;

  function f
   input Integer i;
   output Real x;
  algorithm
   x:=C.d[i].b;
  end f;

  parameter Integer j = 1;
  Real y = f(j);
 end ConstantLookup33;

 model ConstantLookup34
  package A
   constant B[2] x = {B({1,2}),B({3,4})};

   record B
    Real[2] y;
   end B;
  end A;

  function f
   input Integer i;
   output Real x;
  algorithm
   x:=A.x[i].y[i];
  end f;

  parameter Integer j = 1;
  Real z = f(j);
 end ConstantLookup34;

 model ConstantLookup35
  package A = B
  D b(  redeclare package C = A);

  model D
   replaceable package C = B constrainedby E
   parameter Integer j = 1;
   Real z = C.F.f1(j);
  end D;

  package G
   replaceable package H = I
   partial function f1
    input Integer i;
    output Real x;
   end f1;
  end G;

  package B
      extends E(   redeclare package F = J);
  end B;

  package J
      extends K(   redeclare package H = L);
  end J;

  package K
      extends G;

   redeclare function extends f1
   protected
    Real y = H.a[i];
   algorithm
    x:=y;
   end f1;
  end K;

  package L
      extends I(a = {1,2});
  end L;

  package I
   constant Real[2] a = {3,4};
  end I;

  package E
   replaceable package F = G  end E;
 end ConstantLookup35;

 model ConstantLookup36
  record B
   Real[2] b;
  end B;

  function f
   input Integer i;
   output Real x;
  protected
   constant B[2] a = {B({1,2}),B({3,4})};
  algorithm
   x:=a[i].b[i];
  end f;

  parameter Integer j = 1;
  Real z = f(j);
 end ConstantLookup36;

 model ConstantLookup37
  package A
   constant Real e = 2;

   package B
    constant Real g = 1;
        extends C(f = e);
   end B;
  end A;

  package C
   constant Real f = 1;
  end C;

  package D = A.B(g = 3)
  Real z = D.f;
 end ConstantLookup37;

 model ConstantLookup38
  package A
   constant Real e = 2;

   package B
    constant Real g = 1;
    constant Real f = e;
   end B;
  end A;

  package D = A.B(g = 3)
  Real z = D.f;
 end ConstantLookup38;

 package ExtraForConstantLookup39
  package B
      extends C(f = e);
  end B;

  package C
   constant Real f = 1;
  end C;
 end ExtraForConstantLookup39;

 model ConstantLookup39
  package D = ExtraForConstantLookup39.B
  Real z = D.f;
 end ConstantLookup39;

 model ConstantLookup40
  model A
   constant Real x = 2;
  end A;

  parameter Real y = A.x;
 end ConstantLookup40;

 model ConstantLookup41
  class P1
   Real x;
  end P1;

  package P2
      extends P1;
  end P2;

  Real x = P2.x;
 end ConstantLookup41;

 model ConstantLookup42
  class P1
   constant Real x;
  equation
x = 1;
  end P1;

  package P2
      extends P1;
  end P2;

  Real x = P2.x;
 end ConstantLookup42;

 class ExtendsTest1
  class C
   Real x;
  end C;

  class C2
      extends C;

   class C3
        extends C;
   equation
x = 1;
   end C3;

   C3 c3;
  equation
x = 2;
  end C2;

    extends C;
  C2 c2;
 equation
x = 3;
 end ExtendsTest1;

 class ExtendsTest2
  class C
   class D
    Real x;
   end D;
  end C;

    extends C;
  D d;
 equation
d.x = 3;
 end ExtendsTest2;

 model ExtendsTest3
  class C
   class D
    Real x;
   end D;
  end C;

    extends C;
    extends D;
 end ExtendsTest3;

 model ExtendsTest4
  package A
   model C
    Real d = 1;
   end C;

   model B
        extends A.C;
   end B;
  end A;

  A.B e[2];
  Real y;
 equation
y = e[1].d;
 end ExtendsTest4;

 model ImportTest1
  package P
   model A
    Real x;
   end A;

   model B
    Real y;
   end B;
  end P;

  import NameTests.ImportTest1.P.*;
  A a;
    extends A;
  B b;
    extends B;
 end ImportTest1;

 model ImportTest2
  package P
   model A
    Real x;
   end A;

   model B
    Real y;
   end B;
  end P;

  import NameTests.ImportTest2.P.A;
  A a;
    extends A;
 end ImportTest2;

 model ImportTest3
  package P
   model A
    Real x;
   end A;

   model B
    Real y;
   end B;
  end P;

  import NameTests.ImportTest1.P.A;
  A a;
    extends A;
  B b;
 end ImportTest3;

 model ImportTest4
  package P
   model A
    Real x;
   end A;
  end P;

  import PP = NameTests.ImportTest1.P;
  PP.A a;
    extends PP.A;
 end ImportTest4;

 model ImportTest5
  package P
   model A
    Real x = 0;
   end A;

   model B
    Real y = 1;
   end B;

   model C
    model D
     Real z = 2;
    end D;
   end C;
  end P;

  import NameTests.ImportTest5.P.C.*;
  D d(z = 3);
 end ImportTest5;

 model ImportTest6
  package P
   model M
    import SI = NameTests.ImportTest6.P.SIunits;
    SI.Resistance R(start = 1);
   end M;

   package SIunits
    type Resistance = Real(unit = "Ohm")   end SIunits;
  end P;

  P.M m;
 end ImportTest6;

 model ImportTest7
  package P
   package P1
    import SI = NameTests.ImportTest7.P.SIunits;

    model M
     SI.Resistance R(start = 1);
    end M;
   end P1;

   package SIunits
    type Resistance = Real(unit = "Ohm")   end SIunits;
  end P;

  P.P1.M m;
 end ImportTest7;

 model ImportTest9
  import Math = Modelica.Math;
  parameter Real p1 = Math.cos(9);
  parameter Real p2 = Modelica.Math.sin(9);
  parameter Real p3 = sqrt(3);
 end ImportTest9;

 model ImportTest10
  model A
   import C = Modelica.Constants;
   Real x = C.pi;
  end A;

  A y;
 end ImportTest10;

 model ImportTest11
  import mue_0 = Modelica.Constants.mue_0;
  import SI = Modelica.SIunits;

  model M
   SI.Voltage v = 0;
   Real x = mue_0;
  end M;

  M m;
 end ImportTest11;

 model ImportTest12
  import SI = NotHere.SI;
  SI.Voltage v = 0;
 end ImportTest12;

 model ShortClassDeclTest1
  model A
   Real x = 2;
  end A;

  model AA = A
  AA aa;
 end ShortClassDeclTest1;

 model ShortClassDeclTest2
  model A
   Real x = 2;
  end A;

  model AA = A
  model AAA = AA
  AAA aa;
 end ShortClassDeclTest2;

 model ShortClassDeclTest3
  type MyReal = Real(min = -3)
  MyReal x(start = 3);
 end ShortClassDeclTest3;

 model ShortClassDeclTest31
  Modelica.SIunits.Angle x(start = 3);
 end ShortClassDeclTest31;

 model ShortClassDeclTest35_Err
  type MyReal = Real(min = -3, q = 4)
  MyReal x(start = 3, t = 5);
 end ShortClassDeclTest35_Err;

 model ShortClassDeclTest4
  connector MyRealInput = MyRealSignal(max = 5)
  connector MyRealSignal
   replaceable type SignalType = Real(unit = "V")
      extends SignalType(nominal = 34);
  end MyRealSignal;

  MyRealInput u(min = 3);
 end ShortClassDeclTest4;

 model ShortClassDeclTest5
  connector MyRealInput = MyRealSignal(start = 3, nominal = 3)
  type MyReal = Real(unit = "V")
  connector MyRealSignal
   replaceable type SignalType = MyReal(min = 1, max = 1, start = 1, nominal = 1)
      extends SignalType(max = 2, start = 2, nominal = 2);
  end MyRealSignal;

  MyRealInput u(nominal = 4);
 end ShortClassDeclTest5;

 model ShortClassDeclTest6
  model Resistor
   parameter Real R;
  end Resistor;

  parameter Real R = 1;

  replaceable model Load = Resistor(R = R)
  Load a;
 end ShortClassDeclTest6;

 model ShortClassDeclTest8
  connector RealInput = Real
  RealInput u;
  Modelica.Blocks.Interfaces.RealInput u2;
 end ShortClassDeclTest8;

 model ShortClassDeclTest9
  package B = A1
  package A2
   function f
    output Real x = 1;
   algorithm
   end f;
  end A2;

  Real y = B.f();
 end ShortClassDeclTest9;

 model DerTest1
  Real x;
 equation
x = 1;
 end DerTest1;

 model InitialEquationTest1
  Real x;
 initial equation
x = 1;
 equation
x = 1;
 end InitialEquationTest1;

 model EndExpTest1
  Real x[1];
 equation
x[end] = 2;
 end EndExpTest1;

 model ForTest1
  Real x[3,3];
 equation
i in 1:3j in 1:3x[i,j] = i+j;
 end ForTest1;

 model ForTest2_Err
  Real x[3,3];
 equation
i in 1:3j in 1:3x[i,j] = i+k;
 end ForTest2_Err;

 model StateSelectTest
  Real x(stateSelect = StateSelect.never);
  Real y(stateSelect = StateSelect.avoid);
  Real z(stateSelect = StateSelect.default);
  Real w(stateSelect = StateSelect.prefer);
  Real v(stateSelect = StateSelect.always);
 equation
x = 2;
y = 1;
z = 1;
w = 1;
v = 1;
 end StateSelectTest;

 model IndexLookup1
  model B
   Real z[2] = {1,2};
  end B;

  parameter Integer i = 2;
  B y;
  Real x = y.z[i];
 end IndexLookup1;

 model IndexLookup2
  model B
   parameter Integer i = 1;
   Real z[2] = {1,2};
  end B;

  parameter Integer i = 2;
  B y;
  Real x = y.z[i];
 end IndexLookup2;

 model ConditionalComponentTest1_Err
  parameter Real x = 1 if 1;
 end ConditionalComponentTest1_Err;

 model ConditionalComponentTest2_Err
  parameter Boolean b[2] = {true,true};
  parameter Real x = 1 if b;
 end ConditionalComponentTest2_Err;

 model ConditionalComponentTest3_Err
  parameter Integer b[2] = {1,1};
  parameter Real x = 1 if b;
 end ConditionalComponentTest3_Err;

 model ConditionalComponentTest4
  parameter Boolean b = true;
  parameter Real x = 1 if b;
 end ConditionalComponentTest4;

 model ConditionalComponentTest5
  package P
   constant Boolean b = true;
  end P;

  parameter Real x = 1 if P.b;
 end ConditionalComponentTest5;

 model ConditionalComponentTest6_Err
  parameter Boolean b = false;
  parameter Real x = 1 if b;
  Real y = x;
 end ConditionalComponentTest6_Err;

 model ConditionalComponentTest7_Err
  model M
   Real x = 2;
  end M;

  parameter Boolean b = false;
  M m if b;
  Real y = m.x;
 end ConditionalComponentTest7_Err;

 model ConditionalComponentTest8
  parameter Boolean b = false;
  parameter Real x = 1 if b;
 end ConditionalComponentTest8;

 model ConditionalComponentTest9
  model N
   Real z;
  equation
z^(2) = 4;
  end N;

  model M
   Real x;
   N n;
      extends N;
  equation
x = 3;
  end M;

  parameter Boolean b = false;
  M m if b;
 end ConditionalComponentTest9;

 model ConditionalComponentTest10
  connector RealInput = Real
  connector RealOutput = Real
  model Source
   RealOutput y = p;
   parameter Real p = 1;
  end Source;

  model Sink
   RealInput u;
  end Sink;

  model M
   parameter Boolean b = true;
   RealInput u1 if b;
   RealInput u2 if not b;
   RealOutput y;
  equation
connect(u1,y);
connect(u2,y);
  end M;

  parameter Boolean b = true;
  M m(b = b);
  Source source1 if b;
  Source source2 if not b;
  Sink sink;
 equation
connect(source1.y,m.u1);
connect(source2.y,m.u2);
connect(m.y,sink.u);
 end ConditionalComponentTest10;

 model ConditionalComponentTest11
  connector RealInput = Real
  connector RealOutput = Real
  model Source
   RealOutput y = p;
   parameter Real p = 1;
  end Source;

  model Sink
   RealInput u;
  end Sink;

  model M
   parameter Boolean b = true;
   RealInput u1 if b;
   RealInput u2 if not b;
   RealOutput y;
  equation
connect(u1,y);
connect(u2,y);
  end M;

  parameter Boolean b = false;
  M m(b = b);
  Source source1 if b;
  Source source2 if not b;
  Sink sink;
 equation
connect(source1.y,m.u1);
connect(source2.y,m.u2);
connect(m.y,sink.u);
 end ConditionalComponentTest11;

 model ConditionalComponentTest12
  model A
   parameter Boolean flag = true;
   B b1 if flag;
   B b2 if not flag;
   Real z1 if flag;
   Real z2 if not flag;
  end A;

  model B
   Real x;
  end B;

  Real y = 1;
  A a(b1(x = y), b2(x = y), z1 = y, z2 = y);
 end ConditionalComponentTest12;

 model ConditionalComponentTest13_Err
  model A
   Real x;
  end A;

  model B
   parameter Boolean flag = true;
   A a1 if flag;
   A a2 if not flag;
  end B;

  Real y1 if b.flag;
  Real y2 if not b.flag;
  B b(a1(x = y1), a2(x = y2));
 end ConditionalComponentTest13_Err;

 model ConditionalComponentTest14
  record R
   parameter Real x;
  end R;

  parameter R r if false;
 end ConditionalComponentTest14;

 model AttributeDot1
  Real x = 1;
  parameter Real p = x.start;
 end AttributeDot1;

 model ComplexExpInDer1
  Real x;
  Real y;
 equation
x*y = 0;
 end ComplexExpInDer1;

 model ComplexExpInDer2
  Real x;
  Real y;
 equation
x+1-y = 0;
 end ComplexExpInDer2;

 model ComplexExpInDer3
  Real x;
  Real y;
 equation
{x,y} = zeros(2);
 end ComplexExpInDer3;

 model EmptyAnnotationTest
  model M
  end M;

  M m;
 end EmptyAnnotationTest;

 model EmptyModificationTest
  model M
   Real x() = 1;
  end M;

  M m;
 end EmptyModificationTest;

 model FunctionCallLeftTest
  function f
   input Real x1;
   output Real y1;
  algorithm
   y1:=2*x1;
  end f;

  Real x;
 algorithm
  (x) := f(1);
 end FunctionCallLeftTest;

 model PreAliasTest1
  discrete Real x;
  discrete Real y;
 initial equation
pre(x) = 42;
 equation
x = y;
x = pre(x)*pre(y)+3.14;
 end PreAliasTest1;

 model AssignedInWhenRecursion
  model A
   parameter Boolean x = true;
  end A;

  model B
   Real y = 2;
  end B;

  parameter Integer n = if a.x then 2 else 3;
  A a;
  B b[n];
 end AssignedInWhenRecursion;

 model WhenInExtendsTest
  model A
   Real x;
   Real y;
   Integer i;
   parameter Boolean b = false;
  equation
i = 1;
y = 2+iby = 4initial();
x = 3time>=3;
  end A;

  model B
      extends A;
  end B;

  B b;
 end WhenInExtendsTest;

 model InheritInputTest1
  connector A
   Real x;
  end A;

  input A a;
 end InheritInputTest1;

 model InheritInputTest2
  connector A
   Real x;
  end A;

  connector B
   A a;
  end B;

  input B b;
 end InheritInputTest2;

 model InheritInputTest3
  connector A
   input Real x;
  end A;

  input A a;
 end InheritInputTest3;

 model InheritInputTest4
  connector A
   output Real x;
  end A;

  input A a;
 end InheritInputTest4;

 model InheritOutputTest1
  connector A
   Real x;
  end A;

  output A a;
 end InheritOutputTest1;

 model InheritOutputTest2
  connector A
   Real x;
  end A;

  connector B
   A a;
  end B;

  output B b;
 end InheritOutputTest2;

 model InheritOutputTest3
  connector A
   output Real x;
  end A;

  output A a;
 end InheritOutputTest3;

 model InheritOutputTest4
  connector A
   input Real x;
  end A;

  output A a;
 end InheritOutputTest4;

 model InheritFlowTest1
  connector A
   Real x;
  end A;

  connector B
   A ap;
   flow A af;
  end B;

  B b1, b2;
 equation
connect(b1,b2);
 end InheritFlowTest1;

 model InheritFlowTest2
  connector A
   flow Real x;
  end A;

  flow A a;
 end InheritFlowTest2;

 model DuplicateVariables1
  model A
   Real x(start = 1, min = 2) = 3;
  end A;

    extends A;
  Real x(start = 1, min = 2) = 3;
 end DuplicateVariables1;

 model DuplicateVariables2
  model A
   Real x;
  equation
x = time;
  end A;

  model B
   A a;
  end B;

    extends B;
  A a;
 end DuplicateVariables2;

 model DuplicateVariables3
  Real x;
  Real x;
 end DuplicateVariables3;

 model DuplicateVariables5
  model A
   Real x(start = 1, min = 2) = 3;
  end A;

  model B
      extends A;
   Real x(start = 1, min = 2) = 3;
  end B;

  B b[2];
 end DuplicateVariables5;

 model DuplicateVariables6
  model A
   Real x;
   Real x;
  end A;

  A a[2];
 end DuplicateVariables6;

 model BadEscape1
  parameter String a = "\qabc\ def\nghi\\xjkl\?mno\#";
 end BadEscape1;

 model BadEscape2
  Real '\qabc\ def\nghi\\xjkl\?mno\#' = 1;
  Real x = '\qabc\ def\nghi\\xjkl\?mno\#';
 end BadEscape2;

 model LibWithVerTest1
  LibWithVer.Test a;
 end LibWithVerTest1;

 model ProtectedComponent1
  model A
   B b;
  protected
   Real y = time;
  end A;

  model B
  protected
   C c;
  end B;

  model C
   Real x = time;
  end C;

  A a;
 end ProtectedComponent1;

 model ClassThroughComponent1
  model A
   package B
    constant Real x = 1;
   end B;

   parameter Real y = 2;
  end A;

  A a;
  parameter Real z = a.B.x*a.y;
 end ClassThroughComponent1;

 model ClassThroughComponent2
  model A
   package B
    constant Real x = 1;
   end B;

   parameter Real y = 2;
  end A;

  model C
   A a;
  end C;

  C c;
  parameter Real z = c.a.B.x*c.a.y;
 end ClassThroughComponent2;

 model ClassThroughComponent3
  model A
   package B
    constant Real x = 1;
   end B;

   parameter Real y = 2;
  end A;

  model C
   A a[2];
  end C;

  C c;
  parameter Real z = c.a[1].B.x*c.a[1].y;
 end ClassThroughComponent3;

 model ClassThroughComponent4
  model A
   package B
    model C
     parameter Real x = 1;
    end C;
   end B;

   parameter Real y = 2;
  end A;

  A a;
  a.C c;
 end ClassThroughComponent4;
end NameTests;
