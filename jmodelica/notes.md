# Things to get compiler working

- copied contents of
	- `$JMODLICA_SVN/trunk/Compiler/ModelicaFrontend/src/` into `$THIS_REPO/src/main`
	- `$JMODLICA_SVN/trunk/Compiler/ModelicaCompiler/src/` into `$THIS_REPO/src/main`
	- `$JMODLICA_SVN/trunk/Compiler/ModelicaFlatTree/src/jastadd/` into `$THIS_REPO/src/main/jastadd/flat`
- use our standard `build.gradle` with package `org.jmodelica`
- for parser and scanner, replace `AST_PACKAGE` with `org.jmodelica.compiler`, and `PARSER_PACKAGE` with `org.jmodelica.parser` (got this information from `build-case.xml`)
- copy `JMODLICA_SVN/trunk//Compiler/ModelicaCompiler/runtime.options` and `JMODLICA_SVN/trunk//Compiler/ModelicaCompiler/module.options` into `$THIS_REPO/src/main/resources/`
- add a new target `genExtraMacro` in the `build.gradle`
	- resembled from the `build.xml`, especially from the macro-def `gen-extra-macro` 
- manually copied some parts needed for source-analysis, but were defined somewhere else, including:
	- replaceMe from FunctionInlining, defined in MiddleEnd
	- class Printer, defined in flat
