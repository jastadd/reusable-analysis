package de.tudresden.inf.st.relast.reuse;

import de.tudresden.inf.st.relast.reuse.model.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jgrapht.Graph;
import org.jgrapht.alg.connectivity.KosarajuStrongConnectivityInspector;
import org.jgrapht.alg.interfaces.StrongConnectivityAlgorithm;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class Main {

  private static Logger logger = LogManager.getLogger(Main.class);

  public static void main(String[] args) {
    logger.info("running main");

    // create simple state machine

    StateMachine stateMachine = new StateMachine();

    // add states A to G

    State a = new State();
    a.setLabel("a");
    stateMachine.addElement(a);
    stateMachine.setInitial(a);
    State b = new State();
    b.setLabel("b");
    stateMachine.addElement(b);
    State c = new State();
    c.setLabel("c");
    stateMachine.addElement(c);
    State d = new State();
    d.setLabel("d");
    stateMachine.addElement(d);
    State e = new State();
    e.setLabel("e");
    stateMachine.addElement(e);
    stateMachine.addFinal(e);
    State f = new State();
    f.setLabel("f");
    stateMachine.addElement(f);
    stateMachine.addFinal(f);
    State g = new State();
    g.setLabel("g");
    stateMachine.addElement(g);
    stateMachine.addFinal(g);

    // add transitions
    {
      Transition af = new Transition();
      af.setFrom(a);
      af.setTo(f);
      af.setLabel("af");
      stateMachine.addElement(af);
    }

    Transition ab = new Transition();
    ab.setFrom(a);
    ab.setTo(b);
    ab.setLabel("ab");
    stateMachine.addElement(ab);

    Transition bc = new Transition();
    bc.setFrom(b);
    bc.setTo(c);
    bc.setLabel("bc");
    stateMachine.addElement(bc);

    Transition bd = new Transition();
    bd.setFrom(b);
    bd.setTo(d);
    bd.setLabel("bd");
    stateMachine.addElement(bd);

    Transition ce = new Transition();
    ce.setFrom(c);
    ce.setTo(e);
    ce.setLabel("ce");
    stateMachine.addElement(ce);

    Transition eb = new Transition();
    eb.setFrom(e);
    eb.setTo(b);
    eb.setLabel("eb");
    stateMachine.addElement(eb);

    Transition ed = new Transition();
    ed.setFrom(e);
    ed.setTo(d);
    ed.setLabel("ed");
    stateMachine.addElement(ed);

    Transition fa = new Transition();
    fa.setFrom(f);
    fa.setTo(a);
    fa.setLabel("fa");
    stateMachine.addElement(fa);

    Transition fg = new Transition();
    fg.setFrom(f);
    fg.setTo(g);
    fg.setLabel("fg");
    stateMachine.addElement(fg);

    Transition gg = new Transition();
    gg.setFrom(g);
    gg.setTo(g);
    gg.setLabel("gg");
    stateMachine.addElement(gg);
    {

      Set<Set<State>> smSCC = stateMachine.SCC();
      Set<Set<Component>> dgComponentSCCType = stateMachine.dependencyGraph().SCC();
      Set<Set<State>> dgSCC = dgComponentSCCType.stream()
          .map(scc -> scc.stream().map(Component::getState).collect(Collectors.toSet()))
          .collect(Collectors.toSet());

      List<Set<Component>> dgJGraphTSCCType = jgraphtComputeSCC(stateMachine.dependencyGraph());
      Set<Set<State>> jgSCC = dgJGraphTSCCType.stream()
          .map(scc -> scc.stream().map(Component::getState).collect(Collectors.toSet()))
          .collect(Collectors.toSet());

      System.out.println("\nType Dependency analysis:");
      System.out.println("  StateMachine SCC: " + smSCC);
      System.out.println("  DepGrap SCC:      " + dgSCC);
      System.out.println("  JGraphT SCC:      " + jgSCC);
      System.out.println("  SCCs are equal:   " + smSCC.equals(dgSCC));
      System.out.println("  SCCs are equal:   " + smSCC.equals(jgSCC));
      System.out.println("\n\n");


//      try {
//
//        new File("graph/statemachine/dot/").mkdirs();
//        int sccIndex = 0;
//        for (Set<Component> scc : dgComponentSCCType) {
//          if (scc.size() > 1) {
//            PrintWriter writer = new PrintWriter("graph/statemachine/dot/scc" + sccIndex + ".dot");
//            writer.println(stateMachine.dependencyGraph().dotGraph(scc).toDot());
//            writer.flush();
//            writer.close();
//            sccIndex++;
//          }
//        }
//
//        new File("graph/statemachine/puml/").mkdirs();
//        sccIndex = 0;
//        for (Set<Component> scc : dgComponentSCCType) {
//          if (scc.size() > 1) {
//            PrintWriter writer = new PrintWriter("graph/statemachine/puml/scc" + sccIndex + ".puml");
//            writer.println(stateMachine.dependencyGraph().dotGraph(scc).toPlant("class", ".."));
//            writer.flush();
//            writer.close();
//            sccIndex++;
//          }
//        }
//
//      } catch (FileNotFoundException exception) {
//        exception.printStackTrace();
//      }
    }
  }

  /**
   * Computes the set of SCC subgraphs utilizing JGraphT.
   * This function is generic wrt. the actual ASTNode the Component
   * is associated to.
   *
   * @param dg the given DependencyGraph
   * @return a list of subgraphs of the SCCs
   **/
  private static List<Set<Component>> jgraphtComputeSCC(DependencyGraph dg) {
    Graph<Component, DefaultEdge> directedGraph = new DefaultDirectedGraph<>(DefaultEdge.class);
    for (Component c : dg.getComponentList()) {
      directedGraph.addVertex(c);
    }
    for (Component to : dg.getComponentList()) {
      for (Component from : to.getFromList()) {
        directedGraph.addEdge(from, to);
      }
    }
    // computes all the strongly connected components of the directed graph
    StrongConnectivityAlgorithm<Component, DefaultEdge> alg =
        new KosarajuStrongConnectivityInspector<>(directedGraph);
    List<Set<Component>> scc = alg.stronglyConnectedSets();
    return scc;
  }
}
