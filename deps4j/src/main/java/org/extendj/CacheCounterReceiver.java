package org.extendj;

import org.extendj.ast.ASTNode;
import org.extendj.ast.ASTState;

import java.io.PrintStream;
import java.util.*;

public class CacheCounterReceiver implements ASTState.Trace.Receiver {

  private SortedMap<String, Integer> cacheRead = new TreeMap<>();
  private SortedMap<String, Integer> cacheWrite = new TreeMap<>();
  private SortedMap<String, Integer> flushAttr = new TreeMap<>();


  private SortedMap<String, Integer> circularCase1Start = new TreeMap<>();
  private SortedMap<String, Integer> circularCase1Change = new TreeMap<>();
  private SortedMap<String, Integer> circularCase1Return = new TreeMap<>();
  private SortedMap<String, Integer> circularCase2Start = new TreeMap<>();
  private SortedMap<String, Integer> circularCase2Change = new TreeMap<>();
  private SortedMap<String, Integer> circularCase2Return = new TreeMap<>();
  private SortedMap<String, Integer> circularCase3Return = new TreeMap<>();

  public void printStats(PrintStream ostream) {
    List<String> attributes = new ArrayList<>();
    attributes.addAll(cacheWrite.keySet());
    Collections.sort(attributes);
    ostream.println("\nAttribute Name,Cache Write,cacheRead,CacheFlushes,Cache Hit Rate (read/(read+write))");
    for (String attribute: attributes) {
      int write = cacheWrite.getOrDefault(attribute,0);
      int read = cacheRead.getOrDefault(attribute,0);
      int flush = flushAttr.getOrDefault(attribute,0);
      double hitRate = 1.0*read/(read+write);
      ostream.println(attribute + "," + write + "," + read + "," + flush + "," + hitRate);
    }
    ostream.println("\n\nAttribute Name, 1Start,1Change,1Return, 2Start,2Change,2Return, 3Return");
    attributes.clear();
    attributes.addAll(circularCase1Start.keySet());
    Collections.sort(attributes);
    for (String attribute: attributes) {
      int case1Start  = circularCase1Start.getOrDefault(attribute, 0);
      int case1Change = circularCase1Change.getOrDefault(attribute, 0);
      int case1Return = circularCase1Return.getOrDefault(attribute, 0);
      int case2Start  = circularCase2Start.getOrDefault(attribute, 0);
      int case2Change = circularCase2Change.getOrDefault(attribute, 0);
      int case2Return = circularCase2Return.getOrDefault(attribute, 0);
      int case3Return = circularCase3Return.getOrDefault(attribute, 0);
      ostream.println(attribute + "," + case1Start + "," + case1Change + "," + case1Return + ",  " + case2Start + "," + case2Change + "," + case2Return + ",  " + case3Return);
    }
  }

  @Override
  public void accept(ASTState.Trace.Event event, ASTNode node, String attribute, Object params, Object value) {
    switch (event) {
      case CACHE_READ:
        cacheRead.put(attribute, cacheRead.getOrDefault(attribute,0) + 1);
        break;
      case CACHE_WRITE:
        cacheWrite.put(attribute, cacheWrite.getOrDefault(attribute,0) + 1);
        break;
      case FLUSH_ATTR:
        flushAttr.put(attribute, flushAttr.getOrDefault(attribute,0) + 1);
        break;
      case CIRCULAR_CASE1_START:
        circularCase1Start.put(attribute, circularCase1Start.getOrDefault(attribute, 0) + 1);
        break;
      case CIRCULAR_CASE1_CHANGE:
        circularCase1Change.put(attribute, circularCase1Change.getOrDefault(attribute, 0) + 1);
        break;
      case CIRCULAR_CASE1_RETURN:
        circularCase1Return.put(attribute, circularCase1Return.getOrDefault(attribute, 0) + 1);
        break;
      case CIRCULAR_CASE2_START:
        circularCase2Start.put(attribute, circularCase2Start.getOrDefault(attribute, 0) + 1);
        break;
      case CIRCULAR_CASE2_CHANGE:
        circularCase2Change.put(attribute, circularCase2Change.getOrDefault(attribute, 0) + 1);
        break;
      case CIRCULAR_CASE2_RETURN:
        circularCase2Return.put(attribute, circularCase2Return.getOrDefault(attribute, 0) + 1);
        break;
      case CIRCULAR_CASE3_RETURN:
        circularCase3Return.put(attribute, circularCase3Return.getOrDefault(attribute, 0) + 1);
        break;
    }

  }

  private Map<String, Map<String, Integer>> mapHandlers;

  public Map<String, Map<String, Integer>> getMapHandlers() {
    return mapHandlers;
  }

  public void setMapHandlers(Map<String, Map<String, Integer>> mapHandlers) {
    this.mapHandlers = mapHandlers;
  }
}
