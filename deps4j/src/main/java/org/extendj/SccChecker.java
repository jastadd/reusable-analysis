package org.extendj;

import org.extendj.ast.*;
import org.jgrapht.Graph;
import org.jgrapht.alg.connectivity.KosarajuStrongConnectivityInspector;
import org.jgrapht.alg.interfaces.StrongConnectivityAlgorithm;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class SccChecker extends Frontend {

  public SccChecker() {
    super("Java Checker", ExtendJVersion.getVersion());
  }

  /**
   * maps strings to CheckerType
   **/
  private static CheckerType getCheckerType(String ct) {
    switch (ct) {
      case "internal":
        return CheckerType.INTERNAL;
      case "jgrapht":
        return CheckerType.JGRAPHT;
      default:
        return CheckerType.EXTERNAL;
    }
  }

  /**
   * Computes the set of SCC subgraphs utilizing JGraphT.
   * This function is generic wrt. the actual ASTNode the Component
   * is associated to.
   *
   * @param dg the given DependencyGraph
   * @return a list of subgraphs of the SCCs
   **/
  private static List<Set<Component>> jgraphtComputeSCC(DependencyGraph dg) {
    Graph<Component, DefaultEdge> directedGraph = new DefaultDirectedGraph<>(DefaultEdge.class);
    for (Component c : dg.getComponentList()) {
      directedGraph.addVertex(c);
    }
    for (Component to : dg.getComponentList()) {
      for (Component from : to.getFromList()) {
        directedGraph.addEdge(from, to);
      }
    }
//        System.out.println(directedGraph);
    // computes all the strongly connected components of the directed graph
    StrongConnectivityAlgorithm<Component, DefaultEdge> alg =
        new KosarajuStrongConnectivityInspector<>(directedGraph);
    List<Set<Component>> scc = alg.stronglyConnectedSets();
    //Print SCCs
//        for (Graph<String, DefaultEdge> g:scc){
//            System.out.println("SCC:");
//            System.out.println(g.vertexSet().stream().collect(Collectors.joining(", ")));
//        }
    return scc;
  }

  /**
   * Entry point for the Java checker.
   *
   * @param args command-line arguments
   */
  public static void main(String[] args) {

    if (!((args.length == 1) || (args.length == 3))) {
      System.out.println("usage: SccChecker <type/package/fullpackage> <internal/external/jgrapht> <directory with java files>");
      System.exit(-1);
    }

    if (args.length == 3) {
      // batch mode

      try {
        List<String> files = Files.walk(Paths.get(args[2]))
            .filter(Files::isRegularFile)
            .filter(x -> x.getFileName().toString().endsWith(".java")).map(Path::toString).collect(Collectors.toList());

        // measure the time (with parsing) from here
        long startMeasurementTime = System.nanoTime();

        Program program = new SccChecker().readProgram(files);

        // measure the time (without parsing) from here
        long startGenerationTime = System.nanoTime();

        long startAnalysisTime = startGenerationTime;

        CacheCounterReceiver receiver = new CacheCounterReceiver();

        program.trace().setReceiver(receiver);

        boolean type = args[0].equals("type");
        CheckerType checker = getCheckerType(args[1]);
        boolean internal = args[1].equals("internal");
        boolean considerPackageTree = args[0].equals("fullpackage");


        long endTime;

        if (type) {
          Set<Set<String>> scc;
          // type dependencies
          switch (checker) {
            case INTERNAL:
              scc = program.typeSCC().stream()
                  .map(s -> s.stream()
                      .map(TypeDecl::fullName)
                      .collect(Collectors.toSet()))
                  .collect(Collectors.toSet());
              break;
            case EXTERNAL: {
              DependencyGraph dependencyGraph = program.typeDependencyGraph();
              startAnalysisTime = System.nanoTime();
              scc = dependencyGraph.SCC().stream()
                  .map(s -> s.stream()
                      .map(c -> c.asTypeComponent().getTypeDecl().fullName())
                      .collect(Collectors.toSet()))
                  .collect(Collectors.toSet());
              break;
            }
            case JGRAPHT: {
              DependencyGraph dependencyGraph = program.typeDependencyGraph();
              startAnalysisTime = System.nanoTime();
              //scc=jgraphtComputeSCC(dependencyGraph, c->c.getTypeDecl().fullName()).stream().collect(Collectors.toSet());
              scc = jgraphtComputeSCC(dependencyGraph).stream()
                  .map(s -> s.stream().map(c -> c.asTypeComponent().getTypeDecl().fullName()).collect(Collectors.toSet()))
                  .collect(Collectors.toSet());
              break;
            }
            default:
              throw new IllegalArgumentException("CheckerType was undefined");
          }

          // measure the time until here
          endTime = System.nanoTime();

          System.out.print("java,type,"
              + internal + ","
              + files.size() + ","
              + program.numTypeDecls() + ","
              + program.numTypeDependencies() + ","
              + (program.numTypeDecls() + program.numTypeDependencies()) + ","
              + scc.size() + ",");
        } else {
          Set<Set<String>> scc;
          // package dependencies
          switch (checker) {
            case INTERNAL:
              scc = program.packageSCC(considerPackageTree);
              break;
            case EXTERNAL: {
              DependencyGraph dependencyGraph = program.packageDependencyGraph(considerPackageTree);
              startAnalysisTime = System.nanoTime();
              scc = dependencyGraph.SCC().stream()
                  .map(s -> s.stream()
                      .map(c -> c.asPackageComponent().getPackageReference().getPackage())
                      .collect(Collectors.toSet()))
                  .collect(Collectors.toSet());
              break;
            }
            case JGRAPHT: {
              DependencyGraph dependencyGraph = program.packageDependencyGraph(considerPackageTree);
              startAnalysisTime = System.nanoTime();
              //scc=jgraphtComputeSCC(dependencyGraph, c->c.getPackageReference().getPackage()).stream()
              //  .collect(Collectors.toSet());
              scc = jgraphtComputeSCC(dependencyGraph).stream()
                  .map(s -> s.stream().map(c -> c.asPackageComponent().getPackageReference().getPackage()).collect(Collectors.toSet()))
                  .collect(Collectors.toSet());
              break;
            }
            default:
              throw new IllegalArgumentException("CheckerType was undefined");
          }
//          for (Set<String> s:scc){
//            System.out.println("SCC:");
//            System.out.println(s.stream().collect(Collectors.joining(", ")));
//          }
          // measure the time until here
          endTime = System.nanoTime();

          System.out.print("java,"
              + (considerPackageTree ? "fullpackage" : "package") + ","
              + internal + ","
              + files.size() + ","
              + program.numPackages() + ","
              + program.numPackageDependencies(considerPackageTree) + ","
              + (program.numPackages() + program.numPackageDependencies(considerPackageTree)) + ","
              + scc.size() + ",");
        }

        long parseTime = startGenerationTime - startMeasurementTime;
        long generationTime = startAnalysisTime - startGenerationTime;
        long analysisTime = endTime - startAnalysisTime;
        long fullTime = endTime - startMeasurementTime;

        System.out.print((fullTime / 1000000) + ",");
        System.out.print((parseTime / 1000000) + ",");
        System.out.print((generationTime / 1000000) + ",");
        System.out.print((analysisTime / 1000000) + ",");
        System.out.print(((generationTime + analysisTime) / 1000000) + ",");

        //receiver.printStats(System.out);

      } catch (IOException e) {
        throw new RuntimeException(e);
      }

    } else {
      // debug mode

      try {
        List<String> files = Files.walk(Paths.get(args[0]))
            .filter(Files::isRegularFile)
            .filter(x -> x.getFileName().toString().endsWith(".java")).map(Path::toString).collect(Collectors.toList());
        Program program = new SccChecker().readProgram(files);
        System.out.println("Read " + files.size() + " '.java' input files.");
        System.out.println("found " + program.getNumCompilationUnit() + " compilation units");

        {
          Set<Set<String>> programSCCPackageTrue = program.packageSCC(true);
          Set<Set<String>> programSCCPackageFalse = program.packageSCC(false);

          Set<Set<Component>> dgComponentSCCFullPackage = program.packageDependencyGraph(true).SCC();
          Set<Set<String>> dgSCCPackageTrue = dgComponentSCCFullPackage.stream()
              .map(scc -> scc.stream().map(c -> c.asPackageComponent().getPackageReference().getPackage()).collect(Collectors.toSet()))
              .collect(Collectors.toSet());

          Set<Set<Component>> dgComponentSCCPackage = program.packageDependencyGraph(false).SCC();
          Set<Set<String>> dgSCCPackageFalse = dgComponentSCCPackage.stream()
              .map(scc -> scc.stream().map(c -> c.asPackageComponent().getPackageReference().getPackage()).collect(Collectors.toSet()))
              .collect(Collectors.toSet());

          List<Set<Component>> jgraphtSCCPackageTrue = jgraphtComputeSCC(program.packageDependencyGraph(true));
          Set<Set<String>> jgraphtPackageTrue = jgraphtSCCPackageTrue.stream()
              .map(scc -> scc.stream().map(c -> c.asPackageComponent().getPackageReference().getPackage()).collect(Collectors.toSet()))
              .collect(Collectors.toSet());
          List<Set<Component>> jgraphtSCCPackageFalse = jgraphtComputeSCC(program.packageDependencyGraph(false));
          Set<Set<String>> jgraphtPackageFalse = jgraphtSCCPackageFalse.stream()
              .map(scc -> scc.stream().map(c -> c.asPackageComponent().getPackageReference().getPackage()).collect(Collectors.toSet()))
              .collect(Collectors.toSet());


          System.out.println("\nPackage Dependency analysis:");
          System.out.println("  program SCC(true):     " + programSCCPackageTrue);
          System.out.println("  DepGrap SCC(true):     " + dgSCCPackageTrue);
          System.out.println("  JGraphT SCC(true):     " + jgraphtPackageTrue);
          System.out.println("  SCCs(true) are equal:  " + programSCCPackageTrue.equals(dgSCCPackageTrue));
          System.out.println("  SCCs(true) are equal:  " + dgSCCPackageTrue.equals(jgraphtPackageTrue));
          System.out.println();
          System.out.println("  program SCC(false):    " + programSCCPackageFalse);
          System.out.println("  DepGrap SCC(false):    " + dgSCCPackageFalse);
          System.out.println("  JGraphT SCC(false):    " + jgraphtPackageFalse);
          System.out.println("  SCCs(false) are equal: " + programSCCPackageFalse.equals(dgSCCPackageFalse));
          System.out.println("  SCCs(false) are equal:  " + dgSCCPackageFalse.equals(jgraphtPackageFalse));
          System.out.println("\n\n");


          new File("graph/fullpackage/dot/").mkdirs();
          int sccIndex = 0;
          for (Set<Component> scc : dgComponentSCCFullPackage) {
            if (scc.size() > 1) {
              PrintWriter writer = new PrintWriter("graph/fullpackage/dot/scc" + sccIndex + ".dot");
              writer.println(program.packageDependencyGraph(true).dotGraph(scc).toDot());
              writer.flush();
              writer.close();
            }
            sccIndex++;
          }

          new File("graph/fullpackage/puml/").mkdirs();
          sccIndex = 0;
          for (Set<Component> scc : dgComponentSCCFullPackage) {
            if (scc.size() > 1) {
              PrintWriter writer = new PrintWriter("graph/fullpackage/puml/scc" + sccIndex + ".puml");
              writer.println(program.packageDependencyGraph(true).dotGraph(scc).toPlant("frame", ".."));
              writer.flush();
              writer.close();
            }
            sccIndex++;
          }

          new File("graph/package/dot/").mkdirs();
          sccIndex = 0;
          for (Set<Component> scc : dgComponentSCCPackage) {
            if (scc.size() > 1) {
              PrintWriter writer = new PrintWriter("graph/package/dot/scc" + sccIndex + ".dot");
              writer.println(program.packageDependencyGraph(false).dotGraph(scc).toDot());
              writer.flush();
              writer.close();
            }
            sccIndex++;
          }

          new File("graph/package/puml/").mkdirs();
          sccIndex = 0;
          for (Set<Component> scc : dgComponentSCCPackage) {
            if (scc.size() > 1) {
              PrintWriter writer = new PrintWriter("graph/package/puml/scc" + sccIndex + ".puml");
              writer.println(program.packageDependencyGraph(false).dotGraph(scc).toPlant("frame", ".."));
              writer.flush();
              writer.close();
            }
            sccIndex++;
          }

        }
        {
          Set<Set<TypeDecl>> programSCCType = program.typeSCC();

          Set<Set<Component>> dgComponentSCCType = program.typeDependencyGraph().SCC();
          Set<Set<TypeDecl>> dgSCCType = dgComponentSCCType.stream()
              .map(scc -> scc.stream().map(x -> x.asTypeComponent().getTypeDecl()).collect(Collectors.toSet()))
              .collect(Collectors.toSet());

          List<Set<Component>> dgJGraphTSCCType = jgraphtComputeSCC(program.typeDependencyGraph());
          Set<Set<TypeDecl>> dgSCCType2 = dgJGraphTSCCType.stream()
              .map(scc -> scc.stream().map(x -> x.asTypeComponent().getTypeDecl()).collect(Collectors.toSet()))
              .collect(Collectors.toSet());

          System.out.println("\nType Dependency analysis:");
          System.out.println("  program SCC:     " + programSCCType);
          System.out.println("  DepGrap SCC:     " + dgSCCType);
          System.out.println("  JGraphT SCC:     " + dgSCCType2);
          System.out.println("  SCCs are equal:  " + programSCCType.equals(dgSCCType));
          System.out.println("  SCCs are equal:  " + dgSCCType.equals(dgSCCType2));
          System.out.println("\n\n");

          new File("graph/type/dot/").mkdirs();
          int sccIndex = 0;
          for (Set<Component> scc : dgComponentSCCType) {
            if (scc.size() > 1) {
              PrintWriter writer = new PrintWriter("graph/type/dot/scc" + sccIndex + ".dot");
              writer.println(program.typeDependencyGraph().dotGraph(scc).toDot());
              writer.flush();
              writer.close();
              sccIndex++;
            }
          }

          new File("graph/type/puml/").mkdirs();
          sccIndex = 0;
          for (Set<Component> scc : dgComponentSCCType) {
            if (scc.size() > 1) {
              PrintWriter writer = new PrintWriter("graph/type/puml/scc" + sccIndex + ".puml");
              writer.println(program.typeDependencyGraph().dotGraph(scc).toPlant("class", ".."));
              writer.flush();
              writer.close();
              sccIndex++;
            }
          }
        }

      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

  private Program readProgram(Collection<String> files) throws IOException {


    Program program = new Program();
    program.resetStatistics();
    program.initBytecodeReader(Program.defaultBytecodeReader());
    program.initJavaParser(Program.defaultJavaParser());

    initOptions();

    for (String file : files) {
      program.addSourceFile(file);
    }

    TypeDecl object = program.lookupType("java.lang", "Object");
    if (object.isUnknown()) {
      // If we try to continue without java.lang.Object, we'll just get a stack overflow
      // in member lookups because the unknown (Object) type would be treated as circular.
      System.err.println("Error: java.lang.Object is missing."
          + " The Java standard library was not found.");
      throw new RuntimeException("exiting with unhandled error!");
    }

    return program;
  }

  private enum CheckerType {INTERNAL, EXTERNAL, JGRAPHT}
}
