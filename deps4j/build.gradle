buildscript {
    repositories.mavenLocal()
    repositories.mavenCentral()
    dependencies {
        classpath 'org.jastadd:jastaddgradle:1.13.3'
    }
}

apply plugin: 'java'
apply plugin: 'application'
apply plugin: 'jastadd'
apply plugin: 'idea'

repositories {
    mavenLocal()
}

idea {
    module {
        generatedSourceDirs += file('./src/gen/java')
        sourceDirs += file('../extendj/src/frontend')
        sourceDirs += file('../extendj/src/frontend-main')
    }
}

sourceSets.main {
    java {

        srcDirs "src/gen/java"
        srcDirs '../extendj/src/frontend'
        srcDirs '../extendj/src/frontend-main'
    }
    resources {
        srcDir '../extendj/src/res'
        srcDir jastadd.buildInfoDir
    }
}

dependencies {
    compile 'org.jgrapht:jgrapht-core:1.3.1'
}

jastadd {
    configureModuleBuild()

    modules {

        include("../extendj/jastadd_modules")

        module "dependencyanalysis", {

            imports "java8 frontend"

            jastadd {
                basedir ".."
                include "deps4j/src/gen/jastadd/Java.ast"
                include "deps4j/src/gen/jastadd/Java.jadd"
                include "deps4j/src/main/jastadd/*.jrag"
                include "deps4j/src/main/jastadd/*.jadd"
                include "dg/src/main/jastadd/*.jrag"
                include "dg/src/main/jastadd/*.jadd"

                excludeFrom "java8 frontend", "grammar/ConstructorReference.ast"
                excludeFrom "java8 frontend", "grammar/IntersectionCasts.ast"
                excludeFrom "java8 frontend", "grammar/Lambda.ast"
                excludeFrom "java8 frontend", "grammar/LambdaAnonymousDecl.ast"
                excludeFrom "java8 frontend", "grammar/MethodReference.ast"
                // excludeFrom "java7 frontend", "grammar/BasicTWR.ast"
                excludeFrom "java7 frontend", "grammar/Diamond.ast"
                excludeFrom "java7 frontend", "grammar/Literals.ast"
                excludeFrom "java7 frontend", "grammar/MultiCatch.ast"
                excludeFrom "java7 frontend", "grammar/TryWithResources.ast"
                excludeFrom "java5 frontend", "grammar/Annotations.ast"
                excludeFrom "java5 frontend", "grammar/EnhancedFor.ast"
                excludeFrom "java5 frontend", "grammar/Enums.ast"
                excludeFrom "java5 frontend", "grammar/GenericMethods.ast"
                excludeFrom "java5 frontend", "grammar/Generics.ast"
                excludeFrom "java5 frontend", "grammar/StaticImports.ast"
                excludeFrom "java5 frontend", "grammar/VariableArityParameters.ast"
                excludeFrom "java4 frontend", "grammar/BoundNames.ast"
                excludeFrom "java4 frontend", "grammar/Java.ast"
                excludeFrom "java4 frontend", "grammar/Literals.ast"
                excludeFrom "java4 frontend", "grammar/NTAFinally.ast"

                excludeFrom "java7 frontend", "frontend/JavaVersion.jrag"

                excludeFrom "java5 frontend", "frontend/BytecodeReader.jrag"
                excludeFrom "java7 frontend", "frontend/Variable.jadd"
            }

            java {
                basedir "src/main/java/"
                include "**/*.java"
            }

        }

    }


    // Target module to build:
    module = 'dependencyanalysis'

    astPackage = 'org.extendj.ast'
    parser.name = 'JavaParser'
    scanner.name = 'OriginalScanner'

    genDir = 'src/gen/java'

    parser.genDir = 'src/gen/java/org/extendj/parser'
    scanner.genDir = 'src/gen/java/org/extendj/scanner'

    parser.genDir = 'src/gen/java/org/extendj/parser'
    scanner.genDir = 'src/gen/java/org/extendj/scanner'
//
//	if (project.hasProperty('extraJastAddOptions')) {
//		extraJastAddOptions += project.extraJastAddOptions.split(',') as List
//		print("options: ${extraJastAddOptions}")
//	}

//	jastaddOptions = [ "--rewrite=cnta", "--safeLazy", "--tracing=all" ]
    jastaddOptions = ["--rewrite=cnta", "--safeLazy", "--tracing=api", "--visitCheck=false"]

//	jastaddOptions = [ "--concurrent", "--rewrite=cnta", "--safeLazy" ]
//	jastaddOptions = [ "--concurrent", "--rewrite=cnta", "--safeLazy", "--cache=all" ]
}

run {
    mainClassName = 'org.extendj.SccChecker'
    if (project.hasProperty("appArgs")) {
        args Eval.me(appArgs)
    }
}

task preprocess(type: JavaExec) {
    group = 'Build'
    main = "-jar"

    doFirst {
        delete "src/gen/jastadd"
        mkdir "src/gen/jastadd"
    }

    args = [
            "../tools/relast.jar",
            "src/main/jastadd/ProgramToDG.relast",
            "../dg/src/main/jastadd/DependencyGraph.relast",
            "../dg/src/main/jastadd/DotGraph.relast",
            "../dg/src/main/jastadd/DGtoDotG.relast",
            "../extendj/java8/grammar/ConstructorReference.ast",
            "../extendj/java8/grammar/IntersectionCasts.ast",
            "../extendj/java8/grammar/Lambda.ast",
            "../extendj/java8/grammar/LambdaAnonymousDecl.ast",
            "../extendj/java8/grammar/MethodReference.ast",
//			"../extendj/java7/grammar/BasicTWR.ast",
            "../extendj/java7/grammar/Diamond.ast",
            "../extendj/java7/grammar/Literals.ast",
            "../extendj/java7/grammar/MultiCatch.ast",
            "../extendj/java7/grammar/TryWithResources.ast",
            "../extendj/java5/grammar/Annotations.ast",
            "../extendj/java5/grammar/EnhancedFor.ast",
            "../extendj/java5/grammar/Enums.ast",
            "../extendj/java5/grammar/GenericMethods.ast",
            "../extendj/java5/grammar/Generics.ast",
            "../extendj/java5/grammar/StaticImports.ast",
            "../extendj/java5/grammar/VariableArityParameters.ast",
            "../extendj/java4/grammar/BoundNames.ast",
            "../extendj/java4/grammar/Java.ast",
            "../extendj/java4/grammar/Literals.ast",
            "../extendj/java4/grammar/NTAFinally.ast",

            "--listClass=ArrayList",
//			"--jastAddList=JastAddList",
//			"--serializer=jackson",
            "--useJastAddNames",
            "--file",
//			"--resolverHelper",
            "--grammarName=src/gen/jastadd/Java"
    ]

    inputs.files file("../extendj/java8/grammar/ConstructorReference.ast"),
            file("src/main/jastadd/ProgramToDG.relast"),
            file("../dg/src/main/jastadd/DependencyGraph.relast"),
            file("../dg/src/main/jastadd/DotGraph.relast"),
            file("../dg/src/main/jastadd/DGtoDotG.relast"),
            file("../extendj/java8/grammar/IntersectionCasts.ast"),
            file("../extendj/java8/grammar/Lambda.ast"),
            file("../extendj/java8/grammar/LambdaAnonymousDecl.ast"),
            file("../extendj/java8/grammar/MethodReference.ast"),
            file("../extendj/java7/grammar/BasicTWR.ast"),
            file("../extendj/java7/grammar/Diamond.ast"),
            file("../extendj/java7/grammar/Literals.ast"),
            file("../extendj/java7/grammar/MultiCatch.ast"),
            file("../extendj/java7/grammar/TryWithResources.ast"),
            file("../extendj/java5/grammar/Annotations.ast"),
            file("../extendj/java5/grammar/EnhancedFor.ast"),
            file("../extendj/java5/grammar/Enums.ast"),
            file("../extendj/java5/grammar/GenericMethods.ast"),
            file("../extendj/java5/grammar/Generics.ast"),
            file("../extendj/java5/grammar/StaticImports.ast"),
            file("../extendj/java5/grammar/VariableArityParameters.ast"),
            file("../extendj/java4/grammar/BoundNames.ast"),
            file("../extendj/java4/grammar/Java.ast"),
            file("../extendj/java4/grammar/Literals.ast"),
            file("../extendj/java4/grammar/NTAFinally.ast"),
            file("../tools/relast.jar")
    outputs.files file("src/gen/jastadd/Java.ast"),
            file("src/gen/jastadd/Java.jadd")
}


generateAst.dependsOn preprocess

mainClassName = 'org.extendj.SccChecker'
jar.manifest.attributes 'Main-Class': mainClassName
jar.destinationDir = projectDir

sourceCompatibility = '1.8'
targetCompatibility = '1.8'
