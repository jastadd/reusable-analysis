package org.extendj;

import org.extendj.ast.AbstractFinding;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Set;

public class DCL51JTest extends ScopeAnalysisTest {

  @Test
  void test() {

    // see https://wiki.sei.cmu.edu/confluence/display/java/DCL51-J.+Do+not+shadow+or+obscure+identifiers+in+subscopes

    ScopeAnalysis scopeAnalysis = new ScopeAnalysis();
    Set<AbstractFinding> findings = scopeAnalysis.analyze("src/test/resources/dcl51-j", true, true);

    System.out.println(findings);

//    final String fFieldComplient = "src/test/resources/dcl51-j/FieldComplient.java";
    final String fFieldNoncomplient = "src/test/resources/dcl51-j/FieldNoncomplient.java";
//    final String fVariableCompliant = "src/test/resources/dcl51-j/VariableCompliant.java";
    final String fVariableNoncompliant = "src/test/resources/dcl51-j/VariableNoncompliant.java";

    assertShadow(findings, "val", fFieldNoncomplient, 7, fFieldNoncomplient, 5);
    assertShadow(findings, "i", fVariableNoncompliant, 8, fVariableNoncompliant, 5);

    Assertions.assertEquals(2, findings.size());
  }

}
