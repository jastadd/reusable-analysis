package org.extendj;

import org.extendj.ast.AbstractFinding;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Set;

public class InnerTest extends ScopeAnalysisTest {

  @Test
  void test() {

    ScopeAnalysis scopeAnalysis = new ScopeAnalysis();
    Set<AbstractFinding> findings = scopeAnalysis.analyze("src/test/resources/inner", true, true);

    System.out.println(findings);

    // anonymous class
    assertShadow(findings, "fieldA", "ClassA", 11, "ClassA", 13);
    assertShadow(findings, "fieldA", "ClassA", 13, "ClassA", 3);

    // local inner class
    assertShadow(findings, "fieldA", "ClassA", 27, "ClassA", 29);
    assertShadow(findings, "fieldA", "ClassA", 29, "ClassA", 3);
    assertShadow(findings, "changingVar", "ClassA", 25, "ClassA", 19);

    // static member class
    assertShadow(findings, "fieldA", "ClassA", 37, "ClassA", 35);
    assertShadow(findings, "fieldA", "ClassA", 35, "ClassA", 3);

    // member class
    assertShadow(findings, "fieldA", "ClassA", 44, "ClassA", 42);
    assertShadow(findings, "fieldA", "ClassA", 42, "ClassA", 3);

    // anonymous class defined in other class
    assertShadow(findings, "fieldB", "ClassB", 5, "ClassB", 10);

    // the anonymous class inherited a field
    assertShadow(findings, "fieldB", "ClassB", 10, "ClassA", 4);


    Assertions.assertEquals(11, findings.size());
  }

}
