package org.extendj;

import org.extendj.ast.AbstractFinding;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Set;

public class AntTest extends ScopeAnalysisTest {

  @Test
  void test() {

    ScopeAnalysis scopeAnalysis = new ScopeAnalysis();
    Set<AbstractFinding> findings = scopeAnalysis.analyze("../testprograms/ant", false, false);

    System.out.println(findings);

  }

}
