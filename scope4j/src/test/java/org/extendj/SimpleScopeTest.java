package org.extendj;

import org.extendj.ast.AbstractFinding;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Set;

public class SimpleScopeTest extends ScopeAnalysisTest {

  @Test
  void test() {

    ScopeAnalysis scopeAnalysis = new ScopeAnalysis();
    Set<AbstractFinding> findings = scopeAnalysis.analyze("src/test/resources/simple", true, false);

    assertShadow(findings, "localVarA", "ClassA", 19, "ClassA", 11);
    assertShadow(findings, "localVarB", "ClassA", 33, "ClassA", 12);
    assertShadow(findings, "localVarC", "ClassA", 24, "ClassA", 13);
    assertShadow(findings, "fieldA", "ClassA", 29, "ClassA", 3);
    assertShadow(findings, "fieldB", "ClassA", 36, "ClassA", 4);

    Assertions.assertEquals(5, findings.size());
  }

}
