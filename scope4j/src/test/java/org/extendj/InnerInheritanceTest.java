package org.extendj;

import org.extendj.ast.AbstractFinding;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Set;

public class InnerInheritanceTest extends ScopeAnalysisTest {

  @Test
  void test() {

    ScopeAnalysis scopeAnalysis = new ScopeAnalysis();
    Set<AbstractFinding> findings = scopeAnalysis.analyze("src/test/resources/innerInheritance", true, true);

    assertShadow(findings, "fieldA", "ClassB", 6, "ClassA", 3);
    assertShadow(findings, "fieldB", "ClassB", 7, "ClassB", 3);

    Assertions.assertEquals(2, findings.size());
  }

}
