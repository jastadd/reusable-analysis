package org.extendj;

import org.extendj.ast.AbstractFinding;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Set;

public class ProtectedTest extends ScopeAnalysisTest {

  @Test
  void test() {

    ScopeAnalysis scopeAnalysis = new ScopeAnalysis();
    Set<AbstractFinding> findings = scopeAnalysis.analyze("src/test/resources/protected_", true, true);

    System.out.println(findings);

    final String classA = "src/test/resources/protected_/one/ClassA.java";
    final String classB = "src/test/resources/protected_/one/ClassB.java";
    final String classC = "src/test/resources/protected_/one/ClassC.java";
    final String classD = "src/test/resources/protected_/two/ClassD.java";
    final String classE = "src/test/resources/protected_/two/ClassE.java";

    // within class A
    // A.A()
    assertShadow(findings, "fPrivate", classA, 19, classA, 9);
    assertShadow(findings, "fPackage", classA, 20, classA, 10);
    assertShadow(findings, "fProtected", classA, 21, classA, 11);
    assertShadow(findings, "fPublic", classA, 22, classA, 12);
    // A.foo()
    assertShadow(findings, "fPrivate", classA, 26, classA, 9);
    assertShadow(findings, "fPackage", classA, 27, classA, 10);
    assertShadow(findings, "fProtected", classA, 28, classA, 11);
    assertShadow(findings, "fPublic", classA, 29, classA, 12);
    // A.bar()
    assertShadow(findings, "fPrivate", classA, 32, classA, 9);
    assertShadow(findings, "fPackage", classA, 33, classA, 10);
    assertShadow(findings, "fProtected", classA, 34, classA, 11);
    assertShadow(findings, "fPublic", classA, 35, classA, 12);

    // within class B
    assertShadow(findings, "fPackage", classB, 14, classA, 10);
    assertShadow(findings, "fProtected", classB, 15, classA, 11);
    assertShadow(findings, "fPublic", classB, 16, classA, 12);
    // B.B()
    assertShadow(findings, "fPrivate", classB, 19, classB, 13);
    assertShadow(findings, "fPackage", classB, 20, classB, 14);
    assertShadow(findings, "fProtected", classB, 21, classB, 15);
    assertShadow(findings, "fPublic", classB, 22, classB, 16);
    // B.foo()
    assertShadow(findings, "fPrivate", classB, 26, classB, 13);
    assertShadow(findings, "fPackage", classB, 27, classB, 14);
    assertShadow(findings, "fProtected", classB, 28, classB, 15);
    assertShadow(findings, "fPublic", classB, 29, classB, 16);
    // B.bar()
    assertShadow(findings, "fPrivate", classB, 32, classB, 13);
    assertShadow(findings, "fPackage", classB, 33, classB, 14);
    assertShadow(findings, "fProtected", classB, 34, classB, 15);
    assertShadow(findings, "fPublic", classB, 35, classB, 16);

    // within class C
    // C.C()
    assertShadow(findings, "fPackage", classC, 20, classA, 10);
    assertShadow(findings, "fProtected", classC, 21, classA, 11);
    assertShadow(findings, "fPublic", classC, 22, classA, 12);
    // C.foo()
    assertShadow(findings, "fPackage", classC, 27, classA, 10);
    assertShadow(findings, "fProtected", classC, 28, classA, 11);
    assertShadow(findings, "fPublic", classC, 29, classA, 12);
    // C.bar()
    assertShadow(findings, "fPackage", classC, 33, classA, 10);
    assertShadow(findings, "fProtected", classC, 34, classA, 11);
    assertShadow(findings, "fPublic", classC, 35, classA, 12);

    // within class D
    assertNotShadow(findings, "fPackage", classD, 20, classA, 10);
    assertShadow(findings, "fProtected", classD, 15, classA, 11);
    assertShadow(findings, "fPublic", classD, 16, classA, 12);
    // D.D()
    assertNotShadow(findings, "fPrivate", classD, 19, classA, 9); // true, but not helpful
    assertNotShadow(findings, "fPackage", classD, 20, classA, 10); // true, but not helpful
    assertShadow(findings, "fProtected", classD, 21, classD, 15);
    assertShadow(findings, "fPublic", classD, 22, classD, 16);
    // D.foo()
    assertNotShadow(findings, "fPrivate", classD, 26, classA, 9); // true, but not helpful
    assertNotShadow(findings, "fPackage", classD, 27, classA, 10); // true, but not helpful
    assertShadow(findings, "fProtected", classD, 28, classD, 15);
    assertShadow(findings, "fPublic", classD, 29, classD, 16);
    // D.bar()
    assertNotShadow(findings, "fPrivate", classD, 32, classA, 9); // true, but not helpful
    assertNotShadow(findings, "fPackage", classD, 33, classA, 10); // true, but not helpful
    assertShadow(findings, "fProtected", classD, 34, classD, 15);
    assertShadow(findings, "fPublic", classD, 35, classD, 16);

    // within class E
    // E.E()
    assertNotShadow(findings, "fPackage", classE, 20, classA, 10);
    assertShadow(findings, "fProtected", classE, 21, classA, 11);
    assertShadow(findings, "fPublic", classE, 22, classA, 12);
    // E.foo()
    assertNotShadow(findings, "fPackage", classE, 27, classA, 10);
    assertShadow(findings, "fProtected", classE, 28, classA, 11);
    assertShadow(findings, "fPublic", classE, 29, classA, 12);
    // E.bar()
    assertNotShadow(findings, "fPackage", classE, 33, classA, 14);
    assertShadow(findings, "fProtected", classE, 34, classA, 11);
    assertShadow(findings, "fPublic", classE, 35, classA, 12);

    Assertions.assertEquals(56, findings.size());
  }

}
