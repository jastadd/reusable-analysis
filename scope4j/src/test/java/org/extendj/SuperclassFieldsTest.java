package org.extendj;

import org.extendj.ast.AbstractFinding;
import org.extendj.ast.VariableShadowFinding;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Set;

public class SuperclassFieldsTest extends ScopeAnalysisTest {

  @Test
  void test() {

    ScopeAnalysis scopeAnalysis = new ScopeAnalysis();
    Set<AbstractFinding> findings = scopeAnalysis.analyze("src/test/resources/superclassFields", false, false);


    assertShadow(findings, "fieldC", "ClassB", 19, "ClassB", 3);
    assertShadow(findings, "fieldB", "ClassB", 21, "ClassB", 2);
    assertShadow(findings, "fieldB", "ClassB", 2, "ClassA", 4);

    Assertions.assertEquals(3, findings.size());

  }

}
