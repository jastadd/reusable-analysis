package org.extendj;

import org.extendj.ast.AbstractFinding;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Set;

public class InterfaceTest extends ScopeAnalysisTest {

  @Test
  void test() {


    final String classA = "src/test/resources/interface/ClassA.java";
    final String interfaceA = "src/test/resources/interface/InterfaceA.java";
    final String interfaceB = "src/test/resources/interface/InterfaceB.java";
    final String interfaceAorB = "src/test/resources/interface/Interface";

    ScopeAnalysis scopeAnalysis = new ScopeAnalysis();
    Set<AbstractFinding> findings = scopeAnalysis.analyze("src/test/resources/interface", true, true);

    assertShadow(findings, "fieldA", classA,     2, interfaceA, 2);
    assertShadow(findings, "fieldB", classA,     3, interfaceB, 3);
    assertShadow(findings, "fieldC", classA, 4, interfaceAorB, 4);

    Assertions.assertEquals(3, findings.size());
  }

}
