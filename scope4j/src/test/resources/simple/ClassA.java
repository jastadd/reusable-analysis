public abstract class ClassA {

  int fieldA;
  int fieldB;

  public ClassA(int constructorParameterA) {
    int localConstructorVarA = 0;
  }

  public void methodNameA(int parameterA) {
    int localVarA = 1;
    int localVarB = 1;
    int localVarC = 1;

    {
      int localVarInBlockA = 2;

      // this is shadowing (and forbidden)
      int localVarA = 3;
    }

    class Local {
      {
        for (int localVarC = 0; localVarC < 10; localVarC++) System.out.println(localVarC);
      }
    }

    // this is shadowing (over two levels, not forbidden)
    int fieldA;

    try (
        // this is forbidden
        java.util.zip.ZipFile localVarB = new java.util.zip.ZipFile("zipFileName");

        // this is okay
        java.io.BufferedWriter fieldB = java.nio.file.Files.newBufferedWriter(null)
    ) { /* do stuff */ } catch (java.io.IOException e) {/* do stuff */}
  }

  // these do not appear as a scope (and, more importantly, the parameters are not added anywhere else)
  public abstract void methodNameB(int parameterForAbstractMethodB);
  public abstract void methodNameC(int fieldA);
}
