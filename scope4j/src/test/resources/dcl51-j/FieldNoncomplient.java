/**
 * Noncompliant Code Example (Field Shadowing)
 */
public class FieldNoncomplient {
  private int val = 1;
  private void doLogic() {
    int val;
    //...
  }
}
