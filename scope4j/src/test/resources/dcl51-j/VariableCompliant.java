/**
 * Compliant Solution (Variable Shadowing)
 */
public class VariableCompliant {
  private void doLogic() {
    for (int i = 0; i < 10; i++) {/* ... */}
    for (int i = 0; i < 20; i++) {/* ... */}
  }
}
