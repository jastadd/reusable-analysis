/**
 * Noncompliant Code Example (Variable Shadowing)
 */
public class VariableNoncompliant {
  private int i = 0;
  private void doLogic() {
    for (i = 0; i < 10; i++) {/* ... */}
    for (int i = 0; i < 20; i++) {/* ... */}
  }
}


