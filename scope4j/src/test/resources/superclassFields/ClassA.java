public abstract class ClassA {

  int fieldA;
  int fieldB;

  abstract void m();

  void n() {
    //...
  }

}
