public class ClassB extends ClassA {
  int fieldB;
  int fieldC;

  @Override
  void m() {
    //Overridden..
  }

  void n() {
    //not overriden
  }

  void n(int value) {
    //polymorphic
  }

  class ClassC {
    int fieldC;

    public ClassC(int fieldB) {
      fieldC = fieldB;
    }
  }
​
}
