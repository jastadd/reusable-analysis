/*
 As of https://docs.oracle.com/javase/tutorial/java/package/namingpkgs.html, an underscore shall be added to
 avoid conflicts with Java keywords.
*/
package protected_.one;

class ClassB extends ClassA {





  private int fPrivate = 0;
  int fPackage = 0;
  protected int fProtected = 0;
  public int fPublic = 0;

  ClassB() {
    int fPrivate = 1;
    int fPackage = 1;
    int fProtected = 1;
    int fPublic = 1;
  }

  void foo() {
    int fPrivate = 2;
    int fPackage = 2;
    int fProtected = 2;
    int fPublic = 2;
  }

  void bar(int fPrivate,
           int fPackage,
           int fProtected,
           int fPublic) {
    // empty
  }
}
