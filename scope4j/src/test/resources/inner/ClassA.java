public abstract class ClassA {

  int fieldA;
  int fieldB;

  abstract void toBeDefined();

  void method1() {
    ClassA anonymous = new ClassA() {
      void toBeDefined() {
        int fieldA = 11;
      }
      int fieldA = 1;
    };
  }

  void method2() {
    final int finalVar = 1;
    int changingVar = 0;
    changingVar = 1;  // changingVar is not-final and not-effective-final, thus can not be used in InnerA
    class InnerA extends ClassA {
      /* This variable shares the name, but actually could never reference the outer scope
         We include it anyway, because a) it would obscure analysis for this edge-case, and b) warns for potentially
          unwanted effects (as all shadowing-warnings do) */
      int changingVar = 4;
      void toBeDefined() {
        int fieldA = 21 + changingVar + finalVar;
      }
      int fieldA = 2;
    }
    ClassA inner = new InnerA();
  }

  static class StaticMemberClass extends ClassA {
    int fieldA = 3;
    void toBeDefined() {
      int fieldA = 31;
    }
  }

  class MemberClass extends ClassA {
    int fieldA = 4;
    void toBeDefined() {
      int fieldA = 41;
    }
  }
}
