public class ClassB {
  void anonymousClassFromOtherSourceFile() {
    ClassA anonymous = new ClassA() {
      void toBeDefined() {
        int fieldB = 11;
      }

      /* false-negative. there should be two scopes: ClassA and this method of ClassB
       * But there is only the method, thus, not shadowing of ClassA.fieldB is detected. */
      int fieldB = 1;
    }
  }
}
