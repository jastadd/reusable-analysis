package org.jmodelica;

import org.jmodelica.compiler.AbstractFinding;
import org.jmodelica.compiler.Declaration;
import org.jmodelica.compiler.MultipleDeclarationFinding;
import org.jmodelica.compiler.VariableShadowFinding;
import org.junit.jupiter.api.Assertions;

import java.util.Set;

public abstract class ScopeAnalysisTest {
  static void assertShadow(Set<AbstractFinding> findings, String name, int shadowerLine, int shadowedLine) {
    for (AbstractFinding finding : findings) {
      if (finding instanceof VariableShadowFinding) {
        Declaration shadower = ((VariableShadowFinding)finding).getShadower();
        Declaration shadowed = ((VariableShadowFinding)finding).getShadowed();
        if (shadowed.getName().equals(name) && shadowed.lineNumber() == shadowedLine && shadower.lineNumber() == shadowerLine) {
          return;
        }
      }
    }
    Assertions.fail("No shadow finding found for name '" + name + "' in lines " + shadowerLine + " > " + shadowedLine);
  }

  static void assertRedefinition(Set<AbstractFinding> findings, String name, int declLine) {
    for (AbstractFinding finding : findings) {
      if (finding instanceof MultipleDeclarationFinding) {
        Declaration declaration = ((MultipleDeclarationFinding)finding).getDeclaration();
        if (declaration.getName().equals(name) && declaration.lineNumber() == declLine) {
          return;
        }
      }
    }
    Assertions.fail("No multi-decl finding found for name '" + name + "' in line " + declLine);
  }
}
