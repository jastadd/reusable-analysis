package org.jmodelica;

import org.jmodelica.compiler.AbstractFinding;
import org.junit.jupiter.api.Test;

import java.util.Set;

public class EncapsulationTest extends ScopeAnalysisTest {

  @Test
  void test() {
    ScopeAnalysis scopeAnalysis = new ScopeAnalysis();
    Set<AbstractFinding> findings = scopeAnalysis.analyze("src/test/resources/encapsulated", true, false);
  }

}
