package org.jmodelica;


import beaver.Parser;
import org.jmodelica.compiler.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.*;
import java.util.stream.Collectors;
import org.jmodelica.util.exceptions.CompilerException;

public class ScopeAnalysis {



  public SourceRoot getSourceRoot() {
    return sourceRoot;
  }

  private SourceRoot sourceRoot;

  /**
   * Entry point for the Java checker.
   *
   * @param args command-line arguments
   */
  public static void main(String[] args) {

    List<String> arguments = new ArrayList<>(Arrays.asList(args));

    boolean debug = arguments.isEmpty() || arguments.remove("--debug");
    boolean tree = arguments.remove("--tree");
    boolean warnings = arguments.remove("--warnings");
    boolean ast = arguments.remove("--ast");

    if (arguments.size() > 1) {
      System.out.println("usage: ScopeAnalysis [--debug] [--tree] [--warnings] [--ast] <directory with modelica files>");
      System.exit(-1);
    }
    String path = arguments.isEmpty() ? "../testprograms/modelica/simple" : arguments.get(0);

    if (debug) {
      new ScopeAnalysis().analyze(path, tree, warnings, ast);
    } else {
      new ScopeAnalysis().analyzeTimed(path);
    }

  }

  public void analyzeTimed(String path) {
    try {
      List<String> files = Files.walk(Paths.get(path))
          .filter(Files::isRegularFile)
          .filter(x -> x.getFileName().toString().endsWith(".mo")).map(Path::toString).collect(Collectors.toList());

      // measure the time (with parsing) from here
      long startMeasurementTime = System.nanoTime();

      sourceRoot = readProgram(files);

      // measure the time (without parsing) from here
      long startGenerationTime = System.nanoTime();

      RootScope scopeTree = sourceRoot.scopeTree();

      long startAnalysisTime = System.nanoTime();

      Set<AbstractFinding> findings = scopeTree.variableShadowings();

      // measure the time until here
      long endTime = System.nanoTime();

      System.out.print("modelica,inner,false,"
          + files.size() + ","
          + scopeTree.numScopes() + ","
          + scopeTree.numDeclarations() + ","
          + (scopeTree.numScopes() + scopeTree.numDeclarations()) + ","
          + findings.size() + ",");

      long parseTime = startGenerationTime - startMeasurementTime;
      long generationTime = startAnalysisTime - startGenerationTime;
      long analysisTime = endTime - startAnalysisTime;
      long fullTime = endTime - startMeasurementTime;

      System.out.print((fullTime / 1000000) + ",");
      System.out.print((parseTime / 1000000) + ",");
      System.out.print((generationTime / 1000000) + ",");
      System.out.print((analysisTime / 1000000) + ",");
      System.out.print(((generationTime + analysisTime) / 1000000) + ",");

    } catch (IOException | Parser.Exception | CompilerException e) {
      throw new RuntimeException(e);
    }
  }

  public Set<AbstractFinding> analyze(String path, boolean tree, boolean warnings) {
    return analyze(path, tree, warnings, false);
  }

  public Set<AbstractFinding> analyze(String path, boolean tree, boolean warnings, boolean ast) {
    try {
      List<String> files = Files.walk(Paths.get(path))
          .filter(Files::isRegularFile)
          .filter(x -> x.getFileName().toString().endsWith(".mo")).map(Path::toString).collect(Collectors.toList());

      sourceRoot = readProgram(files);

      RootScope scopeTree = sourceRoot.scopeTree();

      if (tree) {
        scopeTree.printAST();

      }

      if (ast) {
        sourceRoot.printAST();
      }

      if (warnings) {
        // TODO find out if there are compiler warnings in JModelica
        System.out.println("Currently, compiler warnings are not supported for jModelica.");
      }

      Set<AbstractFinding> findings = scopeTree.variableShadowings();

      System.out.println("\nScope4J found the following problems:");
      for (AbstractFinding finding : findings) {
        System.out.println(finding);
      }
      System.out.println();

      return findings;
    } catch (IOException | Parser.Exception | CompilerException e) {
      System.out.println("Current relative path is: " + Paths.get("").toAbsolutePath());
      throw new RuntimeException(e);
    }
  }

  private static SourceRoot readProgram(Collection<String> files) throws IOException, beaver.Parser.Exception, CompilerException {
    ModelicaCompiler mc = new ModelicaCompiler(ModelicaCompiler.createOptions());
    return mc.getParserHandler().parseModel(UtilInterface.create(mc), (files.toArray(new String[]{})));
  }
}
